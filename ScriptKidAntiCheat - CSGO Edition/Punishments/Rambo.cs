﻿using ScriptKidAntiCheat.Classes.Utils;
using ScriptKidAntiCheat.Utils;
using System;
using System.Threading;
using static ScriptKidAntiCheat.Utils.MouseHook;

/*
 * This script switches weapon to knife whenever the player tries to shoot
 * 
 * It's quite a small script but would surely aggravate any cheater
 * whom this would happen to
 */
namespace ScriptKidAntiCheat.Punishments
{
    public class Rambo : Punishment
    {
        private bool RamboModeIsActive = false;

        private bool HasChanged = false;

        public Rambo() : base(0) // 0 = Always active
        {
            MouseHook.MouseAction += Event;
        }

        private void Event(object MouseEvent, EventArgs e)
        {
            try
            {
                if (!Program.GameProcess.IsValidAndActiveWindow || !Program.GameData.Player.IsAlive() || Program.GameData.MatchInfo.isFreezeTime) return;

                // Read directly from memory 
                bool CanShoot = Program.GameData.Player.checkIfCanShoot(Program.GameProcess);

                Weapons ActiveWeapon = (Weapons)Player.ActiveWeapon;

                RamboModeIsActive = (ActiveWeapon != Weapons.Knife_CT && ActiveWeapon != Weapons.Knife_T);

                if (CanShoot == false || RamboModeIsActive == false) return;

                if ((MouseEvents)MouseEvent == MouseEvents.WM_LBUTTONDOWN)
                {
                    ActivatePunishment();
                }
            }
            catch (Exception ex)
            { }
        }

        private void ActivatePunishment()
        {
            if (base.CanActivate() == false) return;

            if (RamboModeIsActive != HasChanged)
            {
                HasChanged = RamboModeIsActive;
                if (RamboModeIsActive)
                {
                    Program.GameConsole.SendCommand("slot3");
                    RamboModeIsActive = false;
                }
            }
        }
    }
}

